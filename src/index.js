import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  RouterProvider
} from "react-router-dom"
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Home from './Components/Layout/Home';
import Blog from './Components/Blog/Blog';
import Details from './Components/Blog/Details';
import Index123 from './Components/Blog/Index123';
import Index456 from './Components/Member/Index456';
import Update from './Components/Member/Update';
import Addproduct from './Components/Product/Addproduct';
import Myproduct from './Components/Product/Myproduct';
import Editproduct from './Components/Product/Editproduct';
import Productdetails from './Components/Layout/Productdetails';
import Cart from './Components/Layout/Cart';
import Wishlist from './Components/Layout/Wishlist';
import {Provider} from "react-redux"
import store from './Store';
import App1 from './App1';
import Example from './Example';
import Index from './page/Homepages/Index';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <Example/>
  // <div>
  //     <Provider store={store}>
  //       <Index/>
  //     </Provider>
  // </div>
  <React.StrictMode>
    {/* <Test /> */}
    <Router>
      <App>
        <Routes>
          <Route index path="/" element={<Home />} />
          <Route path="/blog" element={<Blog />} />
          <Route path='/login' element={<Index456 />} />
          <Route path='/account/update' element={<Update />} />
          <Route path='/account/add' element={<Addproduct />} />
          <Route path='/account/my-product' element={<Myproduct />} />
          <Route path="/blog-details/:id" element={<Details />} />
          <Route path="/product-details/:id" element={<Productdetails/>} />
          <Route path='/wishlist' element={<Wishlist/>}/>
          <Route path='/cart' element={<Cart />} />
          <Route path='/element' element={<Example />} />
          <Route path="/account/my-product/product/edit/:id" element={<Editproduct />} />
        </Routes>
      </App>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
