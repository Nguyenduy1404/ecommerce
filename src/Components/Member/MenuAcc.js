import { Link } from "react-router-dom"

function MenuAcc() {
    return (
        <div className="col-sm-3">
            <div className="left-sidebar">
                <h2>Category</h2>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h4 className="panel-title"><a href="#">Account</a></h4>
                    </div>
                    <div className="panel-heading">
                        <h4 className="panel-title"><Link to="/account/my-product" >My product</Link></h4>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default MenuAcc