import { useEffect, useState } from "react"
import axios from "axios"
function Update(props) {
    let getDataUser = JSON.parse(localStorage.getItem("DataUser"))
    const [inputs, setInputs] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: "",
    })
    const [getAvatar, setAvatar] = useState("")
    const [getFile, setFile] = useState("")
    const [errors, setErrors] = useState({})
    function renderError() {
        if (Object.keys(errors).length > 0) {
            return Object.keys(errors).map((key, index) => {
                return (
                    <p key={index}>{errors[key]}</p>
                )
            })
        }
    }
    useEffect(() =>{
        let getDataUser = JSON.parse(localStorage.getItem("DataUser"))
            setInputs({
            name: getDataUser["user"].name,
            email: getDataUser["user"].email,
            phone: getDataUser["user"].phone,
            address: getDataUser["user"].address,
        })
    },[])
    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInputs(state => ({ ...state, [nameInput]: value }))
    }
    function handleFile(e) {
        const file = e.target.files
        setFile(e.target.files)
        let reader = new FileReader()
        reader.onload = (e) => {
            setAvatar(e.target.result)
        }
        reader.readAsDataURL(file[0])
        console.log(reader)
    }
    function fetchData() {
            return (
                <form action="#" encType="multipart/form-data" onSubmit={handleSubmit} >
                    <input type="text" name="name" placeholder="Name"  value={inputs.name} onChange={handleInput} />
                    <input type="email" name="email" placeholder="Email Address" value={inputs.email} readonly  />
                    <input type="password" name="password" placeholder="Password" />
                    <input type="text" name="phone" placeholder="Phone" value={inputs.phone} onChange={handleInput} />
                    <input type="text" name="address" placeholder="Address" value={inputs.address} onChange={handleInput} />
                    <input type="text" name="level" placeholder="Level" value={"0"} />
                    <input type="file" name="Avatar" onChange={handleFile} />
                    <button type="submit" className="btn btn-default">Update</button>
                </form>
            )
    }
    function handleSubmit(e){
        let errorSubmit = {}
        e.preventDefault()
        let flag = true
        if (getFile != ""){
            let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"]
            let getsize = getFile[0]["size"]
            let getname = getFile[0]["name"]
            let test = getname.split(".")
            let test1 = checkImg.includes(test[1])
            if (getsize > 1024 * 1024) {
                errorSubmit.files = "File qua lon"
                flag = false
            } else if (!checkImg.includes(test[1])) {
                errorSubmit.files = "Sai dinh dang"
                flag = false
            }
        }
        if (!flag) {
            setErrors(errorSubmit)
        }
        if (flag == true){
        let url = "http://localhost/laravel8/public/api/user/update/" +getDataUser["user"].id
        let accessToken = getDataUser.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        }
        const formData = new FormData()
        formData.append("name", inputs.name)
        formData.append("email", inputs.email)
        formData.append("password", inputs.password ? inputs.password : 0)
        formData.append("phone", inputs.phone)
        formData.append("address", inputs.address)
        formData.append("level", 0)
        formData.append("avatar", getAvatar ? getAvatar : 0)
        axios.post(url, formData, config)
            .then(response => {
                console.log(response)
                setErrors(response.data.response)
            })
        }
    }
    return (
        <div className="row">
            <div className="col-sm-1">
            </div>
            <div className="col-sm-4">
                <div className="signup-form">{/*sign up form*/}
                {renderError()}
                    <h2>User update</h2>
                    {fetchData()}
                </div>
            </div>
        </div>
    )
}
export default Update