import { useState } from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"

function Login() {
    const navigate = useNavigate()
    const [inputs, setInputs] = useState({
        email: "",
        password: "",
    })
    const [errors, setErrors] = useState({})
    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInputs(state => ({ ...state, [nameInput]: value }))
    }
    function renderError() {
        if (Object.keys(errors).length > 0) {
            return Object.keys(errors).map((key, index) => {
                return (
                    <p key={index}>{errors[key]}</p>
                )
            })
        }
    }
    function handleSubmit(e) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        e.preventDefault()
        let errorSubmit = {}
        let flag = true
        if (inputs.email == "") {
            errorSubmit.email = "Vui long nhap email"
            flag = false
        } else {
            if (!regex.test(inputs.email)) {
                errorSubmit.email = "Nhap dung dinh dang"
            }
        }
        if (inputs.password == "") {
            errorSubmit.pass = "Vui long nhap pass"
            flag = false
        }
        if (!flag) {
            setErrors(errorSubmit)
        }
        if (flag) {
            const data1 = {
                email: inputs.email,
                password: inputs.password,
                level: 0
            }
            axios.post("http://localhost/laravel8/public/api/login", data1)
                .then(res => {
                    if (res.data.errors) {
                        setErrors(res.data.errors)
                    } else {
                        console.log(res)
                        const data = {
                            user: res.data.Auth,
                            token: res.data.token
                        }
                        console.log(data)
                        localStorage.setItem("DataUser", JSON.stringify(data))
                        hook: navigate("/")
                    }
                })
        }
    }
    return (
        <div className="col-sm-4 col-sm-offset-1">
            <div className="login-form">{/*login form*/}
                <h2>Login to your account</h2>
                {renderError()}
                <form action="#" onSubmit={handleSubmit} >
                    <input type="text" name="email" placeholder="Email" onChange={handleInput} />
                    <input type="password" name="password" placeholder="Password" onChange={handleInput} />
                    <span>
                        <input type="checkbox" className="checkbox" />
                        Keep me signed in
                    </span>
                    <button type="submit" className="btn btn-default">Login</button>
                </form>
            </div>{/*/login form*/}
        </div>
    )
}
export default Login