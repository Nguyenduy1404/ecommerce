import axios from "axios"
import { useState } from "react"
function Register() {
    const [inputs, setInputs] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: "",
    })
    const [getAvatar, setAvatar] = useState("")
    const [getFile, setFile] = useState("")
    function handleFile(e) {
        const file = e.target.files
        setFile(e.target.files)
        let reader = new FileReader()
        reader.onload = (e) => {
            setAvatar(e.target.result)
        }
        console.log(e.target.files)
        reader.readAsDataURL(file[0])
        console.log(reader)
    }
    const [errors, setErrors] = useState({})
    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInputs(state => ({ ...state, [nameInput]: value }))
    }
    function renderError() {
        if (Object.keys(errors).length > 0) {
            return Object.keys(errors).map((key, index) => {
                return (
                    <p key={index}>{errors[key]}</p>
                )
            })
        }
    }
    function handleSubmit(e) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        e.preventDefault()
        let errorSubmit = {}
        let flag = true
        if (inputs.name == "") {
            errorSubmit.name = "Nhap name"
            flag = false
        }
        if (inputs.email == "") {
            errorSubmit.email = "Vui long nhap email"
            flag = false
        } else {
            if (!regex.test(inputs.email)) {
                errorSubmit.email = "Nhap dung dinh dang"
            }
        }
        if (inputs.password == "") {
            errorSubmit.password = "Vui long nhap pass"
            flag = false
        }
        if (inputs.phone == "") {
            errorSubmit.phone = "Nhap phone"
            flag = false
        }
        if (inputs.address == "") {
            errorSubmit.address = "Nhap address"
            flag = false
        }
        if (getFile == "") {
            errorSubmit.files = "Chon hinh anh"
            flag = false
        } else {
            let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"]
            let getsize = getFile[0]["size"]
            let getname = getFile[0]["name"]
            let test = getname.split(".")
            let test1 = checkImg.includes(test[1])
            if (getsize > 1024 * 1024) {
                errorSubmit.files = "File qua lon"
            } else if (!checkImg.includes(test[1])) {
                errorSubmit.files = "Sai dinh dang"
            }
        }
        if (!flag) {
            setErrors(errorSubmit)
        }
        if (flag) {
            const data = {
                name: inputs.name,
                email: inputs.email,
                password: inputs.password,
                phone: inputs.phone,
                address: inputs.address,
                level: 0,
                avatar: getAvatar
            }
            console.log(data)
            axios.post("http://localhost/laravel8/public/api/register",data)
                .then((res) => {
                    if (res.data.errors) {
                        setErrors(res.data.errors)
                    } else {
                        alert("Dang nhap thanh cong")
                    }
                })
        }
    }
    return (
        <div className="signup-form">{/*sign up form*/}
            <h2>New User Signup!</h2>
            {renderError()}
            <form action="#" encType="multipart/form-data" onSubmit={handleSubmit} >
                <input type="text" name="name" onChange={handleInput} placeholder="Name" />
                <input type="email" name="email" onChange={handleInput} placeholder="Email Address" />
                <input type="password" name="password" onChange={handleInput} placeholder="Password" />
                <input type="text" name="phone" onChange={handleInput} placeholder="Phone" />
                <input type="text" name="address" onChange={handleInput} placeholder="Address" />
                <input type="text" name="level" placeholder="Level" value={"0"} />
                <input type="file" name="Avatar" onChange={handleFile} />
                <button type="submit" className="btn btn-default">Signup</button>
            </form>
        </div>
    )
}
export default Register