import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import StarRatings from "react-star-ratings"

function Rate(props) {
    const [rating, setRating] = useState(0)
    const [data, setData] = useState("")
    const getDataUser = JSON.parse(localStorage.getItem("DataUser"))
    let tong = 0
    useEffect(() => {
        axios.get("http://localhost/laravel8/laravel/public/api/blog/rate/" + props.idBlog)
            .then(response => {
                console.log(response)
                setData(response.data.data)
            })
            .catch(function (error) {
                console.log(error)
            })
    }, [])
    function trungbinhcong() {
        if (Object.keys(data).length > 0) {
            Object.keys(data).map(function (key, index) {
                tong += data[key].rate / Object.keys(data).length
            })
        }
    }
    function changeRating(newRating, name) {
        setRating(newRating)
        let flag = true
        if (!getDataUser) {
            alert("Must login")
            flag = false
        }
        if (flag = true) {
            let url = "http://localhost/laravel8/public/api/blog/rate/" + props.idBlog
            let accessToken = getDataUser.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }
            const formData = new FormData()
            formData.append("user_id", getDataUser.user.id)
            formData.append("blog_id", props.idBlog)
            formData.append("rate", newRating)
            axios.post(url, formData, config)
                .then(response => {
                    console.log(response)
                })
        }
    }
    return (
        <div className="rating-area">
            {trungbinhcong()}
            <StarRatings
                rating={tong}
                starRatedColor="blue"
                changeRating={changeRating}
                numberOfStars={5}
                name='rating'
            />
        </div>
    )
}
export default Rate