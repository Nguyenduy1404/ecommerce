import { Fragment, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import axios from "axios"
import React from 'react';
function Listcomment(props) {
    function getid(e) {
        props.replycmt(e.target.id)
    }
    function rendercomment() {
        let getComment = props.getComment
        if (Object.keys(props).length > 0) {
            if (getComment.length > 0) {
                return getComment.map((value, key) => {
                    if (value.id_comment == 0) {
                        console.log("http://localhost/laravel8/public/upload/user/avatar/" + value.image_user)
                        return (
                            <React.Fragment key={key}>
                                <li className="media">
                                    <a className="pull-left" href="#">
                                        <img style={{widows:"100px"}} className="media-object" src={"http://localhost/laravel8/public/upload/user/avatar/" + value.image_user} alt="" />
                                    </a>
                                    <div className="media-body">
                                        <ul className="sinlge-post-meta">
                                            <li><i className="fa fa-user" />{value.name_user}</li>
                                            <li><i className="fa fa-clock-o" /> {value.updated_at}</li>
                                            <li><i className="fa fa-calendar" /> {value.created_at}</li>
                                        </ul>
                                        <p>{value.comment}</p>
                                        <a className="btn btn-primary" id={value.id} onClick={getid} href><i className="fa fa-reply" />Replay</a>
                                    </div>
                                </li>
                                {
                                    getComment.map((value1, key1) => {
                                        if (value.id == value1.id_comment) {
                                            console.log("http://localhost/laravel8/laravel/public/upload/user/avatar/" + value1.image_user)
                                            return (
                                                <li className="media second-media">
                                                    <a className="pull-left" href="#">
                                                        <img className="media-object" src={"http://localhost/laravel8/laravel/public/upload/user/avatar/" + value1.image_user} alt="" />
                                                    </a>
                                                    <div className="media-body">
                                                        <ul className="sinlge-post-meta">
                                                            <li><i className="fa fa-user" />{value1.name_user}</li>
                                                            <li><i className="fa fa-clock-o" /> {value1.updated_at}</li>
                                                            <li><i className="fa fa-calendar" /> {value1.created_at}</li>
                                                        </ul>
                                                        <p>{value1.comment}</p>
                                                        <a className="btn btn-primary" id={value1.id} onClick={getid} href><i className="fa fa-reply" />Replay</a>
                                                    </div>
                                                </li>
                                            )
                                        }
                                    })
                                }
                            </React.Fragment>
                        )
                    }
                })
            }
        }
    }
    return (
        <div>
            <ul className="media-list">
                {rendercomment()}
            </ul>

        </div>
    )
}
export default Listcomment