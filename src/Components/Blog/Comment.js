import { useState } from "react"
import axios from "axios"
import Listcomment from "./Listcomment"
import Details from "./Details"
function Comment(props) {
    const [comment, setcomment] = useState("")
    const getDataUser = JSON.parse(localStorage.getItem("DataUser"))
    const [errors, setErrors] = useState({})
    function handleContent(e) {
        setcomment(e.target.value)
    }

    function handleSubmit(e) {
        console.log(props)
        let errorsubmit = {}
        let flag = true
        if (!getDataUser) {
            errorsubmit.login = "Must Login"
            flag = false
        } else if (comment == "") {
            errorsubmit.comment = "Must comment"
            flag = false
        } else if (comment) {
            let url = "http://localhost/laravel8/public/api/blog/comment/" + props.idBlog
            let accessToken = getDataUser.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }
            const formData = new FormData()
            formData.append("id_blog", props.idBlog)
            formData.append("id_user", getDataUser.user.id)
            formData.append("id_comment", props.getdata1 ? props.getdata1 : 0)
            formData.append("comment", comment)
            formData.append("image_user", getDataUser.user.avatar)
            formData.append("name_user", getDataUser.user.name)
            console.log(formData)
            axios.post(url, formData, config)
                .then(response => {
                    console.log(response.data.data)
                    props.getcmt(response.data.data)
                })
        }
        if (!flag) {
            setErrors(errorsubmit)
        }
        e.preventDefault()
    }
    function renderError() {
        if (Object.keys(errors).length > 0) {
            return Object.keys(errors).map((key, index) => {
                return (
                    <li key={index}>{errors[key]}</li>
                )
            })
        }
    }
    function replycmt(data1) {
        console.log(data1)
    }
    return (
        <div>
            <div className="replay-box">
                <div className="row">
                    <Listcomment />
                    <div className="col-sm-12">
                        <h2>Leave a replay</h2>
                        <div className="text-area">
                            <div className="blank-arrow">
                                <label>Your Name</label>
                            </div>
                            <span>*</span>
                            {renderError()}
                            <form onSubmit={handleSubmit}>
                                <textarea onChange={handleContent} rows={11}>{comment}</textarea>
                                <button className="btn btn-primary" type="submit">post comment</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Comment