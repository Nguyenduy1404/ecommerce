import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import Comment from "./Comment"
import Listcomment from "./Listcomment"
import Rate from "./Rate"


function Details(props) {
    const [getdata1, setdata1] = useState("")
    let params = useParams()
    const [data, setData] = useState("")
    const [getComment, setcomment] = useState("")
    useEffect(() => {
        axios.get("http://localhost/laravel8/public/api/blog/detail/" + params.id)
            .then(response => {
                console.log(response.data.data)
                setData(response.data.data)
                setcomment(response.data.data.comment)
            })
            .catch(function (error) {
                console.log(error)
            })
    }, [])
    function fetchData() {
        if (Object.keys(data).length > 0) {
            return (
                <div className="single-blog-post">
                    <h3>{data["title"]}</h3>
                    <div className="post-meta">
                        <ul>
                            <li><i className="fa fa-user" /> Mac Doe</li>
                            <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                            <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                        </ul>
                    </div>
                    <a href>
                        <img src={"http://localhost/laravel8/public/upload/Blog/image/" + data["image"]} alt="" />
                    </a>
                    <p>{data["content"]}</p>
                    <div className="pager-area">
                        <ul className="pager pull-right">
                            <li><a href="#">Pre</a></li>
                            <li><a href="#">Next</a></li>
                        </ul>
                    </div>
                </div>
            )
        }

    }
    function getcmt(data) {
        const concatter = getComment.concat(data)
        setcomment(concatter)
    }
    function replycmt(data1) {
        console.log(data1)
        setdata1(data1)
    }
    return (
        <div className="col-sm-9">
            <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                {fetchData()}
            </div>{/*/blog-post-area*/}
            <Rate idBlog={params.id} />
            <div className="socials-share">
                <a href><img src="images/blog/socials.png" alt="" /></a>
            </div>
            <div className="response-area">
                <h2>3 RESPONSES</h2>
                <Listcomment getComment={getComment} replycmt={replycmt} />
            </div>
            <Comment getcmt={getcmt} idBlog={params.id} getdata1={getdata1} />
        </div>
    )
}
export default Details