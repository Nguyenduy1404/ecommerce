import axios from "axios"
import { useEffect, useState, useContext } from "react"
import { Link } from "react-router-dom"
import { UserContext } from "../../UserContext"

function Wishlist(){
    const getdatalocal = JSON.parse(localStorage.getItem("Wishlist"))
    const [data,setData] = useState("")
    let main = []
    const {getwishlist,setwishlist} = useContext(UserContext)
    useEffect(() => {
        const getdatalocal = JSON.parse(localStorage.getItem("Wishlist"))
        axios.get("http://localhost/laravel8/public/api/product/wishlist")
        .then(response => {
            // console.log(response.data.data)
            setData(response.data.data)
        })
        .catch(function (error) {
            console.log(error)
        })
    },[])
    const handledelete = (e) =>{
        const name = e.target.id
        console.log(main)
        if (getdatalocal.length>0){
            for(var key in getdatalocal){
                if(name == getdatalocal[key]){
                    getdatalocal.splice(key,1)
                    setwishlist(getwishlist-1)
                    localStorage.setItem("tongwishlist",JSON.stringify(getwishlist-1))
                    localStorage.setItem("Wishlist",JSON.stringify(getdatalocal))
                }
            }
        }
    }
    function fetchData(){
        if(main.length>0){
            return main.map((value,key)=>{
                const getdataimage = JSON.parse(value.image)
                let urlimage = "http://localhost/laravel8/public/upload/product/" +value.id_user
                return(
                    <div className="col-sm-4">
                    <div className="product-image-wrapper">
                    <div className="single-products">
                        <div className="productinfo text-center">
                            <img src= {urlimage+"/"+getdataimage[0]} alt="" />
                            <h2>{value.price}</h2>
                            <p>{value.name}</p>                                        
                        </div>
                        <div className="product-overlay">
                            <div className="overlay-content">
                                <h2>{value.price}</h2>
                                <p>{value.name}</p>
                            </div>
                        </div>
                    </div>
                    <div className="choose">
                        <ul className="nav nav-pills nav-justified">
                            <li><a onClick={handledelete} id={value.id} href="#"><i className="fa fa-plus-square" />Delete wishlist</a></li>
                            <Link style={{float:"right"}} to={"/product-details/" + value.id}> Read more</Link>
                        </ul>
                    </div>
                    </div>
                    </div>
                )
            })
        }
    }
    function check(){
        if(data.length>0){
                return data.map((value,key)=>{
                    for(var key1 in getdatalocal){
                        if(value.id == getdatalocal[key1]){
                            main.push(value)
                        }
                    }
            })
        }
    }
    return(
        <div className="col-sm-9 padding-right">
        {check()}
        <div className="features_items">{/*features_items*/}
        {fetchData( )}
        </div>{/*features_items*/}
        </div>
    )
}
export default Wishlist