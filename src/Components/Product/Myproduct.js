import axios from "axios"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
function Myproduct(){
    const [getdata,setdata] = useState("")
    let getDataUser = JSON.parse(localStorage.getItem("DataUser"))    
    useEffect(() => {
        let url = "http://localhost/laravel8/public/api/user/my-product"
        let accessToken = getDataUser.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        }
        axios.get(url,config)
            .then(response => {
                console.log(response)
                setdata(response.data.data)
            })
            .catch(function (error) {
                console.log(error)
            })
    }, [])
    function handleonClick(e){
      console.log(e.target.value)
        let url = "http://localhost/laravel8/public/api/user/product/delete/"+e.target.value
        let accessToken = getDataUser.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        }
      axios.get(url,config)
      .then(response => {
        console.log(response)
        setdata(response.data.data)
      })
      .catch(function (error) {
        console.log(error)
      })
    }
    function fetchData(){
        if (Object.keys(getdata).length > 0) {
            return Object.keys(getdata).map((value,key)=>{
              const getdataimage = JSON.parse(getdata[value].image)
              console.log(getdataimage)
              let urlimage = "http://localhost/laravel8/public/upload/product/"+getdata[value].id_user
              return(
                <tr>
                  <td className="cart_description">
                    <h4><a href>{getdata[value].id}</a></h4>
                  </td>
                  <td className="cart_description">
                    <h4><a href>{getdata[value].name}</a></h4>
                  </td>
                  <td className="cart_product">
                    <a href><img style={{width: "100px"}} src= {""+urlimage+"/"+getdataimage[0]} alt="" /></a>
                  </td>
                  <td className="cart_description">
                    <h4><a href>{getdata[value].price}</a></h4>
                  </td>
                  <td class="cart_delete">
								    <button value={getdata[value].id} onClick={handleonClick}>Delete</button>
							    </td>
                  <td >
                    <Link to={"product/edit/"+getdata[value].id}>Edit</Link>
                  </td>
                </tr>
              )
            })
          }
        }
        return(
          <section id="cart_items">
          <div className="container">
            <div className="table-responsive cart_info">
              <table className="table table-condensed">
                <thead>
                  <tr className="cart_menu">
                    <td className="ID">ID</td>
                    <td className="name">Name</td>
                    <td className="Image">Image</td>
                    <td className="Price">Price</td>
                    <td className="Action">Action</td>
                    <td />
                  </tr>
                </thead>
                <tbody>
                  {fetchData()}
                </tbody>
              </table>
            </div>
          </div>
        </section>
      )
    }
export default Myproduct