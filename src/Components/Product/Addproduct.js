import axios from "axios"
import { useEffect, useState } from "react"
function Addproduct(){
    let getDataUser = JSON.parse(localStorage.getItem("DataUser"))
    const [databrand, setDatabrand] = useState("")
    const [datacategory, setDatacategory] = useState("")
    const [getselectbrand, setselectbrand] = useState("")
    const [getselectcategory, setselectcategory] = useState("")
    const [inputs, setInputs] = useState({
        name: "",
        price: "",
        company: ""
    })
    const [choose,setchoose] = useState("")
    const [comment, setcomment] = useState("")
    const [getAvatar, setAvatar] = useState("")
    const [getFile, setFile] = useState("")
    const [status,getstatus] =useState("")
    const [getsale,setSale] = useState("")
    const [inputSale, setinputSale] = useState(false);
    const handleselectbrand = (e) => {
        setselectbrand(e.target.value)
    }
    const handleselectcategory = (e) => {
        setselectcategory(e.target.value)
    }
    const [errors, setErrors] = useState({})
    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInputs(state => ({ ...state, [nameInput]: value }))
    }
    useEffect(() => {
        axios.get("http://localhost/laravel8/public/api/category-brand")
            .then(response => {
                // console.log(response)
                setDatabrand(response.data.brand)
                setDatacategory(response.data.category)
            })
            .catch(function (error) {
                console.log(error)
            })
    }, [])
    function handleContent(e) {
        setcomment(e.target.value)
    }
    function fetchDatabrand(){
        if(databrand.length > 0) {
            return databrand.map((value, key) => {
                return (
                    <option value={value.id} key={key}>{value.brand}</option>
                )
            })
        }
    }
    function fetchDatacategory(){
        if(datacategory.length > 0) {
            return datacategory.map((value, key) => {
                return (
                    <option value={value.id} key={key}>{value.category}</option>
                )
            })
        }
    }
    function renderError() {
        if (Object.keys(errors).length > 0) {
            return Object.keys(errors).map((key, index) => {
                return (
                    <p key={index}>{errors[key]}</p>
                )
            })
        }
    }
    function handleFile(e) {
        const file = e.target.files
        setFile(e.target.files)
        let reader = new FileReader()
        reader.onload = (e) => {
            setAvatar(e.target.result)
        }
        reader.readAsDataURL(file[0])
        console.log(reader)
    }
   function onchangeSale(e){
        getstatus(e.target.value)
        if(e.target.value == 1){
            setinputSale(true)
            setchoose("Sale")
        }else{
            setinputSale(false)
            setchoose("New")
        }
   }
   function onChangeinputsale(e){
        setSale(e.target.value)
   }
   function showSale(){
        if(inputSale){
            return(
                <input type="text" placeholder="Giam gia" name="sale" onChange={onChangeinputsale}/>
            )
        }else{
            return(
                <div></div>
            )
        }
   }
   function handleSubmit(e){
        e.preventDefault()
        let flag = true
        let errorSubmit = {}
        if (inputs.name == "") {
            errorSubmit.name = "Nhap name"
            flag = false
        }
        if (inputs.price == "") {
            errorSubmit.price = "Nhap price"
            flag = false
        }
        if(getselectbrand == ""){
            errorSubmit.brand = "Nhap brand"
            flag = false
        }
        if(getselectcategory == ""){
            errorSubmit.category = "Nhap category"
            flag = false
        }
        if (inputs.company == "") {
            errorSubmit.company = "Nhap company"
            flag = false
        }
        if(status == ""){
            errorSubmit.choose = "choose"
            flag = false
        }
        if (getFile == "") {
            errorSubmit.files = "Chon hinh anh"
            flag = false
        } else {
            Object.keys(getFile).map((item,i)=>{
                let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"]
                let getsize = getFile[item].size
                let getname = getFile[item].name
                let test = getname.split(".")
                let test1 = checkImg.includes(test[1])
                if (getsize > 1024 * 1024) {
                    errorSubmit.files = "File qua lon"
                } else if (!checkImg.includes(test[1])) {
                    errorSubmit.files = "Sai dinh dang"
                }
            })
        }
        if(inputSale == true){
            if(getsale == ""){
                errorSubmit.sale = "Nhap giam gia"
                flag = false
            }
        }
        if(!flag){
            setErrors(errorSubmit)
            console.log(getselectcategory)
        }
        if (flag){
        let url = "http://localhost/laravel8/public/api/user/product/add"
        let accessToken = getDataUser.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json'
            }
        }
        const formData = new FormData()
        formData.append("name", inputs.name)
        formData.append("price", inputs.price)
        formData.append("category", getselectcategory)
        formData.append("brand", getselectbrand)
        formData.append("company", inputs.company)
        formData.append("detail", comment)
        formData.append("status",status)
        formData.append("sale",getsale ? getsale : 0)
        Object.keys(getFile).map((item,i)=>{
            formData.append("file[]",getFile[item])
        })
        axios.post(url,formData,config)
        .then(response => {
            console.log(response)
        })
        }
    }
    return (    
    <div className="row">
        <div className="col-sm-1">
        </div>
        <div className="col-sm-4">
            <div className="signup-form">{/*sign up form*/}
                <h2>Create product</h2>
                {renderError()}
                <form action="#" encType="multipart/form-data" onSubmit={handleSubmit}  >
                    <input type="text" name="name" placeholder="Name" onChange={handleInput} />
                    <input type="text" name="price" placeholder="Price" onChange={handleInput}  />
                    <select name="brand"onChange={handleselectbrand} >
                        <option value="">Please choose Brand</option>
                    {fetchDatabrand()}
                    </select>
                    <select name="category" onChange={handleselectcategory} >
                        <option value="">Please choose Category</option>
                        {fetchDatacategory()}
                        </select>
                    <select onChange={onchangeSale}>
                        <option value="">Choose</option>
                        <option value="0">New</option>
                        <option value="1">Sale</option>
                    </select>
                    {showSale()}
                    <input type="text" name="company" placeholder="Company profile" onChange={handleInput}  />
                    <input type="file" name="files" onChange={handleFile} multiple />
                    <textarea placeholder="Detail" onChange={handleContent}>{comment}</textarea>
                    <button type="submit" className="btn btn-default">Update</button>
                </form>
            </div>
        </div>
    </div>
    )
}
export default Addproduct