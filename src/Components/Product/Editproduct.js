import { useParams } from "react-router-dom"
import axios from "axios"
import { useEffect, useState } from "react"
function Editproduct(){
    let params = useParams()
    let getDataUser = JSON.parse(localStorage.getItem("DataUser"))
    const [inputs,setInputs] = useState({
        name: "",
        price: "",
        company: "",
        detail: "",
        brand: "",
        category:"",
        id: "",
        id_user:""
    })
    const handlecheck = (e) => {
        const getvalueimg = e.target.value 
        if (e.target.checked){
            setarray(newArray => [...newArray, getvalueimg])
        }else{
            const checkidimg = e.target.id
            delete getarray[checkidimg]
        }
    }
    const [getarray,setarray] = useState([])
    const [getimage,setimage] = useState([])
    const [getsale,setSale] = useState("")
    const [choose,setchoose] = useState("")
    const [inputSale, setinputSale] = useState(false);
    const [getAvatar, setAvatar] = useState("")
    const [status,getstatus] =useState("")
    const [getFile, setFile] = useState("")
    const [databrand, setDatabrand] = useState("")
    const [datacategory, setDatacategory] = useState("")
    const [getselectbrand, setselectbrand] = useState("")
    const [getselectcategory, setselectcategory] = useState("")
    const [errors, setErrors] = useState({})
    useEffect(() => {
        let url = "http://localhost/laravel8/public/api/user/product/"+ params.id
        let accessToken = getDataUser.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        }
        axios.get(url,config)
            .then(response => {
                // console.log(response.data.data)
                setInputs({
                    name: response.data.data.name,
                    price: response.data.data.price,
                    company: response.data.data.company_profile,
                    detail: response.data.data.detail,
                    brand: response.data.data.id_brand,
                    category: response.data.data.id_category,
                    id: response.data.data.id,
                    id_user :response.data.data.id_user

                })
                setimage(response.data.data.image)
            })
            .catch(function (error) {
                console.log(error)
            })
        axios.get("http://localhost/laravel8/public/api/category-brand")
        .then(response => {
            // console.log(response)
            setDatabrand(response.data.brand)
            setDatacategory(response.data.category)
        })
            .catch(function (error) {
            console.log(error)
        })    
    }, [])
    function handleFile(e) {
        const file = e.target.files
        setFile(e.target.files)
        let reader = new FileReader()
        reader.onload = (e) => {
            setAvatar(e.target.result)
        }
        reader.readAsDataURL(file[0])
        console.log(reader)
    }
    const handleselectbrand = (e) => {
        setselectbrand(e.target.value)
    }
    const handleselectcategory = (e) => {
        setselectcategory(e.target.value)
    }
    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInputs(state => ({ ...state, [nameInput]: value }))
    }
    function onChangeinputsale(e){
        setSale(e.target.value)
   }
    function showSale(){
        if(inputSale){
            return(
                <input type="text" placeholder="Giam gia" name="sale" onChange={onChangeinputsale}/>
            )
        }else{
            return(
                <div></div>
            )
        }
   }
    function renderError() {
        if (Object.keys(errors).length > 0) {
            return Object.keys(errors).map((key, index) => {
                return (
                    <p key={index}>{errors[key]}</p>
                )
            })
        }
    }
    function fetchDatacategory(){
        if(datacategory.length > 0) {
            return datacategory.map((value, key) => {
                return (
                    <option value={value.id} key={key}>{value.category}</option>
                )
            })
        }
    }
    function fetchDatabrand(){
        if(databrand.length > 0) {
            return databrand.map((value, key) => {
                return (
                    <option value={value.id} key={key}>{value.brand}</option>
                )
            })
        }
    }
    function onchangeSale(e){
        getstatus(e.target.value)
        if(e.target.value == 1){
            setinputSale(true)
            setchoose("Sale")
        }else{
            setinputSale(false)
            setchoose("New")
        }
   }
    function imagecheckbox(){
        if(Object.keys(getimage).length > 0){
            return Object.keys(getimage).map((value,key)=>{
                let urlimage = "http://localhost/laravel8/public/upload/product/" +inputs.id_user
                return(
                    <div style={{display: "inline-block"}}>
                        <input id={value} onChange={handlecheck} style={{width:"20px", display:"inline-block"}} type="checkbox" value={getimage[value]}/>
                        <img style={{width:"50px"}} src={urlimage +"/" +getimage[value]}></img>
                    </div>         
                )
            })
        }
    }
    function fetchData(){
        return (
            <form action="#" encType="multipart/form-data" onSubmit={handleSubmit} >
                <input type="text" name="name" placeholder="Name"  value={inputs.name} onChange={handleInput} />
                <input type="text" name="price" placeholder="Price"  value={inputs.price} onChange={handleInput} />
                <select name="brand"onChange={handleselectbrand} >
                        <option value="">Please choose Brand</option>
                        {fetchDatabrand()}
                </select>
                <select name="category" onChange={handleselectcategory} >
                        <option value="">Please choose Category</option>
                        {fetchDatacategory()}
                        </select>
                <select onChange={onchangeSale} >
                        <option value="">Choose</option>
                        <option value="0">New</option>
                        <option value="1">Sale</option>
                </select>
                {showSale()}
                {imagecheckbox()}
                <input type="text" name="company" placeholder="company"  value={inputs.detail} onChange={handleInput} />
                <input type="file" name="files" onChange={handleFile} multiple/>
                <textarea value={inputs.detail}></textarea>
                <button type="submit" className="btn btn-default">Update</button>
            </form>
        )
    }
    function handleSubmit(e){
        e.preventDefault()
        let flag = true
        let errorSubmit = {}
        if (inputs.name == "") {
            errorSubmit.name = "Nhap name"
            flag = false
        }
        if (inputs.price == "") {
            errorSubmit.price = "Nhap price"
            flag = false
        }
        if(getselectbrand == ""){
            errorSubmit.brand = "Nhap brand"
            flag = false
        }
        if(getselectcategory == ""){
            errorSubmit.category = "Nhap category"
            flag = false
        }
        if(inputs.company == "") {
            errorSubmit.company = "Nhap company"
            flag = false
        }
        if(status == ""){
            errorSubmit.choose = "Please choose"
            flag = false
        }
        if (getFile == "") {
            errorSubmit.files = "Chon hinh anh"
            flag = false
        } else {
            Object.keys(getFile).map((item,i)=>{
                let checkImg = ["png", "jpg", "jpeg", "PNG", "JPG"]
                let getsize = getFile[item].size
                let getname = getFile[item].name
                let test = getname.split(".")
                let test1 = checkImg.includes(test[1])
                if (getsize > 1024 * 1024) {
                    errorSubmit.files = "File qua lon"
                } else if (!checkImg.includes(test[1])) {
                    errorSubmit.files = "Sai dinh dang"
                }
            })
        }
        if(inputSale == true){
            if(getsale == ""){
                errorSubmit.sale = "Nhap giam gia"
                flag = false
            }
        }
        if(!flag){
            setErrors(errorSubmit)
        }
        if (flag){            
            let url = "http://localhost/laravel8/public/api/user/product/update/"+inputs.id
            let accessToken = getDataUser.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                }
            }
            const formData = new FormData()
            formData.append("name", inputs.name)
            formData.append("price", inputs.price)
            formData.append("category", getselectcategory)
            formData.append("brand", getselectbrand)
            formData.append("company", inputs.company)
            formData.append("detail", inputs.detail)
            formData.append("status",status)
            formData.append("sale",getsale ? getsale : 0)
            Object.keys(getFile).map((item,i)=>{
                formData.append("file[]",getFile[item])
            })
            Object.keys(getarray).map((item,i)=>{
                formData.append("avatarCheckBox[]",getarray[item])
            })
            axios.post(url,formData,config)
            .then(response => {
                console.log(response)
            })
        }
    }
    return(
        <div className="row">
        <div className="col-sm-1">
        </div>
        <div className="col-sm-4">
            <div className="signup-form">{/*sign up form*/}
                <h2>Create product</h2>
                {renderError()}
                {fetchData()}
            </div>
        </div>
    </div>
    )
} 
export default Editproduct