import { useDispatch, useSelector } from "react-redux";
import { addNewHobby } from "../../Action/Hobby";
function Index(props){
    const hobbylist = useSelector(state => state.hobby.list)
    console.log(hobbylist)
    const dispatch = useDispatch()
    function handleClick(){
        const newdata = {
            id:123,
            name: "baovic"
        }
        const action = addNewHobby(newdata)
        dispatch(action)
    }
    return(
        <>
        <button onClick={handleClick}>click</button>
        </>
    )
}
export default Index