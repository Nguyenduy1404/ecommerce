const initialState = {
    // list: 123,
    // activeID: null
}
//Reducer nhận 2 giá trị: state,action
const hobbyReducer = (state = initialState, action) =>{
    console.log(action.type)
    //khi lv trên reducer thì chú ý vấn đề tham chiếu
    switch (action.type){
        case "ADD_HOBBY":{           
            //Dua vao local
            localStorage.setItem("tong123", JSON.stringify(action.payload))
            return{                
                list:action.payload
            }   
        }
        default: 
        return state;
    }
}
export default hobbyReducer