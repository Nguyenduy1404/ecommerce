import { Button,Modal } from 'react-bootstrap';
import {  useState } from "react";
function Example() {
  const [lgShow, setLgShow] = useState(false);
  return (
    <>
      <Button onClick={() => setLgShow(true)}>Zoom</Button>
      <Modal
        size="lg"
        show={lgShow}
        onHide={() => setLgShow(false)}
        aria-labelledby="example-modal-sizes-title-lg"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg">
            <img style={{width:"10%"}} src='https://cdn.24h.com.vn/upload/2-2023/images/2023-05-22/120x90/1684740904-83-thumbnail-width740height555.jpg'></img>
          </Modal.Title>
        </Modal.Header>
      </Modal>
    </>
  );
}
  export default Example