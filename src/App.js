import logo from './logo.svg';
import './App.css';
import Header from './Components/Layout/Header';
import Footer from './Components/Layout/Footer';
import Menuleft from './Components/Layout/Menuleft';
import { useLocation } from 'react-router-dom';
import MenuAcc from './Components/Member/MenuAcc';
import { useState } from 'react';
import { UserContext } from './UserContext';
import {Provider} from "react-redux"
import store from './Store';
function App(props) {
  let params1 = useLocation()
  const [gettong,settong] = useState("")
  const [getwishlist,setwishlist] = useState("")
  
  return (
    <>
    {/* <UserContext.Provider value={{gettong,settong,getwishlist,setwishlist } }> */}
    <Provider store={store}>
      <Header />
      <section>
        <div className='container'>
          <div className='row'>
            {params1["pathname"].includes("account") ? <MenuAcc /> : <Menuleft />}
            {props.children}
          </div>
        </div>
      </section>
      <Footer />
      </Provider>
      {/* </UserContext.Provider> */}
    </>
  );
}

export default App;
